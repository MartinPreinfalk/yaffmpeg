<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de" sourcelanguage="en">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>Hauptfenster</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="25"/>
        <source>Input File</source>
        <translation type="unfinished">Quelle</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="39"/>
        <location filename="mainwindow.ui" line="67"/>
        <source>...</source>
        <translation>Datei auswählen...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="50"/>
        <location filename="mainwindow.ui" line="222"/>
        <source>Output File</source>
        <oldsource>Output Directory</oldsource>
        <translation>Ziel</translation>
    </message>
    <message>
        <source>Convert</source>
        <translation type="vanished">Umwandeln</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="88"/>
        <location filename="mainwindow.ui" line="234"/>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="98"/>
        <location filename="mainwindow.ui" line="264"/>
        <source>Abort</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="121"/>
        <source>Clear Log</source>
        <translation>Anzeige löschen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="142"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="156"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="192"/>
        <location filename="mainwindow.cpp" line="92"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="201"/>
        <source>AboutQt</source>
        <translation>Über Qt</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="210"/>
        <source>Input File...</source>
        <translation>Quelle...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="219"/>
        <source>Output File...</source>
        <oldsource>Output Directory...</oldsource>
        <translation>Ziel...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="243"/>
        <source>Settings...</source>
        <translation>Einstellungen...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="252"/>
        <source>Exit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <source>icons taken from: &quot;Must Have Icons by VisualPharm&quot; license: http://creativecommons.org/licenses/by-nd/3.0/</source>
        <translation type="vanished">verwendet icons von: &quot;Must Have Icons by VisualPharm&quot; license: http://creativecommons.org/licenses/by-nd/3.0/</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="107"/>
        <source>About Qt</source>
        <translation>Über Qt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="113"/>
        <source>Please open source file</source>
        <translation>Bitte die Quell-Datei auswählen</translation>
    </message>
    <message>
        <source>Please choose output directory</source>
        <translation type="vanished">Bitte das Ausgabe-Verzeichnis auswählen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="92"/>
        <source>yaffmpeg (Version %1): https://bitbucket.org/MartinPreinfalk/yaffmpeg
license: GNU General Public License as published by the 
Free Software Foundation, either version 3 of the License
or (at your option) any later version.
You should have received a copy of the GNU General Public License
along with this program.  If not, see &lt;https://www.gnu.org/licenses/&gt;

icons taken from: &quot;Must Have Icons by VisualPharm&quot;
license: http://creativecommons.org/licenses/by-nd/3.0/</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="122"/>
        <source>Please choose output file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="134"/>
        <location filename="mainwindow.cpp" line="191"/>
        <location filename="mainwindow.cpp" line="193"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="134"/>
        <source>Source file &quot;%1&quot; does not exist!</source>
        <translation>Quell-Datei &quot;%1&quot; wurde nicht gefunden!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="149"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="149"/>
        <source>Target file &quot;%1&quot; already exists! Overwrite?</source>
        <translation>Ziel-Datei &quot;%1&quot; existiert bereits! Überschreiben?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="167"/>
        <source>execution running...</source>
        <translation>Ausführung läuft...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="188"/>
        <source>Completed</source>
        <translation>Abgeschlossen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="188"/>
        <source>Execution successfully finished!</source>
        <translation>Ausführung wurde erfolgreich abgeschlossen!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="191"/>
        <source>Execution terminated with error %1!</source>
        <translation>Ausfürung wurde mit Fehler %1 beendet!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="193"/>
        <source>Execution aborted or crashed!</source>
        <translation>Ausführung abgebrochen oder abgestüzt!</translation>
    </message>
    <message>
        <source>Execution crashed!</source>
        <translation type="vanished">Ausführung ist abgestürzt!</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="settings.ui" line="14"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="settings.ui" line="27"/>
        <source>cmd:</source>
        <translation>Kommando:</translation>
    </message>
    <message>
        <location filename="settings.ui" line="41"/>
        <source>args:</source>
        <translation>Argumente:</translation>
    </message>
    <message>
        <location filename="settings.ui" line="55"/>
        <source>postfix:</source>
        <translation>Endung:</translation>
    </message>
    <message>
        <location filename="settings.ui" line="69"/>
        <source>Restore Defaults</source>
        <translation>Standardwerte</translation>
    </message>
    <message>
        <location filename="settings.ui" line="89"/>
        <source>Variables:</source>
        <translation>Variablen:</translation>
    </message>
    <message>
        <location filename="settings.ui" line="96"/>
        <source>OUTPUT - substiutue for output file file</source>
        <translation>OUTPUT wird durch Ausgabe-Dateinamen ersetzt</translation>
    </message>
    <message>
        <location filename="settings.ui" line="116"/>
        <source>INPUT - substiutue for input file</source>
        <translation>INPUT wird durch Quell-Dateinamen ersetzt</translation>
    </message>
</context>
</TS>
