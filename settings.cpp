/*
    yaffmpeg - yet another ffmpeg front-end

    Copyright (C) 2018  Martin Preinfalk

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QSettings>
#include "settings.h"
#include "ui_settings.h"

Settings::Settings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Settings)
{
    ui->setupUi(this);
    // load settings
    QSettings s;
    s.beginGroup("settings");
    ui->cmd->setText(s.value("cmd", DEFAULT_CMD).toString());
    ui->args->setText(s.value("args", DEFAULT_ARGS).toString());
    ui->postfix->setText(s.value("postfix", DEFAULT_POSTFIX).toString());
    s.endGroup();
}

Settings::~Settings()
{
    delete ui;
}

void Settings::on_pushButton_clicked()
{
    ui->cmd->setText(DEFAULT_CMD);
    ui->args->setText(DEFAULT_ARGS);
    ui->postfix->setText(DEFAULT_POSTFIX);
}

QString Settings::get_cmd() {
    return ui->cmd->text();
}

QString Settings::get_args() {
    return ui->args->text();
}

QString Settings::get_postfix() {
    return ui->postfix->text();
}

const Ui::Settings* Settings::get_ui() const {
    return ui;
}
