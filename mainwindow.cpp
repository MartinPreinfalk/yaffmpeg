/*
    yaffmpeg - yet another ffmpeg front-end

    Copyright (C) 2018  Martin Preinfalk

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QString>
#include <QMessageBox>
#include <QFileDialog>
#include <QProcess>
#include <QProgressDialog>
#include <QDebug>
#include <QSettings>
#include <QCloseEvent>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "settings.h"
#include "ui_settings.h"

MainWindow::MainWindow(QString file, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow), term_req_cnt_(0)
{
    ui->setupUi(this);

    connect(ui->output_textEdit, SIGNAL(textChanged()), this, SLOT(update_clear_button_enable_setting()));
    connect(ui->source_file, SIGNAL(textChanged(QString)), this, SLOT(update_convert_action_enable_setting()));
    connect(ui->target_file, SIGNAL(textChanged(QString)), this, SLOT(update_convert_action_enable_setting()));

    // setup external process
    process_ = new QProcess(this);
    connect(process_, SIGNAL(started()), this, SLOT(on_process_started()));
    connect(process_,SIGNAL(readyReadStandardOutput()),this,SLOT(on_process_stdout_ready()));
    connect(process_,SIGNAL(readyReadStandardError()),this,SLOT(on_process_stderr_ready()));
    connect(process_, static_cast<void(QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
        [=](int exitCode, QProcess::ExitStatus exitStatus){ this->on_process_finished(exitCode, exitStatus); });

    QSettings  s;
    s.beginGroup("ui/MainWindow");
    ui->source_file->setText(s.value("source_file", "").toString());
    ui->target_file->setText(s.value("target_dir", "").toString());
    resize(s.value("size", QSize(400, 400)).toSize());
    move(s.value("pos", QPoint(200, 200)).toPoint());
    s.endGroup();

    if (!file.isEmpty()) {
        QFile source_file(file);
        if (source_file.exists()) {
            QFileInfo source_file_info(source_file);
            ui->source_file->setText(file);
            QString target_file_name;
            QTextStream target_file_stream(&target_file_name);
            QString postfix = s.value("postfix", DEFAULT_POSTFIX).toString();
            target_file_stream << source_file_info.absolutePath() << "/" << source_file_info.baseName() << postfix << "_" << time(nullptr) << "." << source_file_info.completeSuffix();
            ui->target_file->setText(target_file_name);
        }
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event) {
    QSettings  s;
    s.beginGroup("ui/MainWindow");
    s.setValue("source_file", ui->source_file->text());
    s.setValue("target_dir", ui->target_file->text());
    s.setValue("size", size());
    s.setValue("pos", pos());
    s.endGroup();
    event->accept();
}

void MainWindow::on_actionAbout_triggered()
{
    qDebug() << __PRETTY_FUNCTION__;
    QMessageBox::about(this, tr("About"), tr("\
yaffmpeg (Version %1): https://bitbucket.org/MartinPreinfalk/yaffmpeg\n\
license: GNU General Public License as published by the \n\
Free Software Foundation, either version 3 of the License\n\
or (at your option) any later version.\n\
You should have received a copy of the GNU General Public License\n\
along with this program.  If not, see <https://www.gnu.org/licenses/>\n\
\n\
icons taken from: \"Must Have Icons by VisualPharm\"\n\
license: http://creativecommons.org/licenses/by-nd/3.0/").arg(VERSION));
}

void MainWindow::on_actionAboutQt_triggered()
{
    qDebug() << __PRETTY_FUNCTION__;
    QMessageBox::aboutQt(this, tr("About Qt"));
}

void MainWindow::on_actionactionSelectSource_triggered()
{
    qDebug() << __PRETTY_FUNCTION__;
    QString source_file = QFileDialog::getOpenFileName(this, tr("Please open source file"), ui->source_file->text(), "*.avi (*.avi);;*.mp4 (*.mp4);;*.mpg (*.mpg);;*.*(*.*)");
    if (!source_file.isNull()) {
        ui->source_file->setText(source_file);
    }
}

void MainWindow::on_actionactionSelectTarget_triggered()
{
    qDebug() << __PRETTY_FUNCTION__;
    QString target_file = QFileDialog::getSaveFileName(this, tr("Please choose output file"), ui->target_file->text());
    if (!target_file.isNull()) {
        ui->target_file->setText(target_file);
    }
}

void MainWindow::on_actionactionConvert_triggered()
{
    qDebug() << __PRETTY_FUNCTION__;
    QString source_file_name(ui->source_file->text());
    QFile source_file(source_file_name);
    if (!source_file.exists()) {
        QMessageBox::critical(this, tr("Error"), tr("Source file \"%1\" does not exist!").arg(source_file_name));
        return;
    }
    QFileInfo source_file_info(source_file);

    QSettings s;
    s.beginGroup("settings");
    QString cmd (s.value("cmd", DEFAULT_CMD).toString());
    QString args = s.value("args", DEFAULT_ARGS).toString();
//    QString postfix = s.value("postfix", DEFAULT_POSTFIX).toString();
    s.endGroup();

    QString target_file_name = ui->target_file->text();
    QFile target_file(target_file_name);
    if (target_file.exists()) {
        auto res = QMessageBox::warning(this, tr("Warning"), tr("Target file \"%1\" already exists! Overwrite?").arg(target_file_name),
                                        QMessageBox::StandardButton::Yes | QMessageBox::StandardButton::No);
        if (res == QMessageBox::StandardButton::No) return;
    }

    QStringList args_list = args.split(" ");
    args_list.replaceInStrings("INPUT", source_file_name);
    args_list.replaceInStrings("OUTPUT", target_file_name);

    qDebug() << cmd << " " << args_list.join(" ");
    process_->start(cmd, args_list);
}

void MainWindow::on_process_started()
{
    qDebug() << __PRETTY_FUNCTION__;
    ui->actionactionConvert->setEnabled(false);
    ui->convert_button->setEnabled(false);
    ui->statusBar->showMessage(tr("execution running..."));
    ui->actionAbort->setEnabled(true);
    ui->abortButton->setEnabled(true);
}

void MainWindow::on_process_stdout_ready()
{
    qDebug() << __PRETTY_FUNCTION__;
    ui->output_textEdit->append(process_->readAllStandardOutput());
}

void MainWindow::on_process_stderr_ready()
{
    qDebug() << __PRETTY_FUNCTION__;
    ui->output_textEdit->append(process_->readAllStandardError());
}

void MainWindow::on_process_finished(int exitCode, QProcess::ExitStatus exitStatus)
{
    qDebug() << __PRETTY_FUNCTION__;
    if (exitStatus == QProcess::NormalExit && exitCode == 0) {
        QMessageBox::information(this, tr("Completed"), tr("Execution successfully finished!"));
    } else {
        if (exitStatus == QProcess::NormalExit) {
            QMessageBox::critical(this, tr("Error"), tr("Execution terminated with error %1!").arg(exitCode));
        } else {
            QMessageBox::critical(this, tr("Error"), tr("Execution aborted or crashed!"));
        }
    }
    ui->actionactionConvert->setEnabled(true);
    ui->convert_button->setEnabled(true);
    ui->actionAbort->setEnabled(false);
    ui->abortButton->setEnabled(false);
    ui->statusBar->clearMessage();
    term_req_cnt_ = 0;
}

void MainWindow::on_actionSettings_triggered()
{
    qDebug() << __PRETTY_FUNCTION__;
    Settings dlg(this);
    dlg.setModal(true);
    int rc = dlg.exec();
    if (rc == QDialog::Accepted) {
        qDebug() << "dialog accepted";
        QSettings s;
        s.beginGroup("settings");
        s.setValue("cmd", dlg.get_ui()->cmd->text());
        s.setValue("args", dlg.get_ui()->args->text());
        s.setValue("postfix", dlg.get_ui()->postfix->text());
        s.endGroup();
    } else {
        qDebug() << "dialog not accepted";
    }
}

void MainWindow::update_clear_button_enable_setting()
{
    qDebug() << __PRETTY_FUNCTION__;
    ui->clear_button->setEnabled(!ui->output_textEdit->toPlainText().isEmpty());
}

void MainWindow::update_convert_action_enable_setting()
{
    qDebug() << __PRETTY_FUNCTION__;
    bool en = !ui->source_file->text().isEmpty() && !ui->target_file->text().isEmpty();
    ui->actionactionConvert->setEnabled(en);
    ui->convert_button->setEnabled(en);
}

void MainWindow::on_actionAbort_triggered()
{
    process_->terminate();
    if (term_req_cnt_++ >= 2) {
        process_->kill();
    }
}
