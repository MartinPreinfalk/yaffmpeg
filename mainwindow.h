/*
    yaffmpeg - yet another ffmpeg front-end

    Copyright (C) 2018  Martin Preinfalk

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QProcess>
#include <QMainWindow>

#define VERSION "0.1.1-0"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QString file, QWidget *parent = 0);
    ~MainWindow();

protected:
    virtual void closeEvent(QCloseEvent *event) override;

private slots:
    void on_actionAbout_triggered();

    void on_actionAboutQt_triggered();

    void on_actionactionSelectSource_triggered();

    void on_actionactionSelectTarget_triggered();

    void on_actionactionConvert_triggered();

    void update_clear_button_enable_setting();

    void update_convert_action_enable_setting();

    void on_actionSettings_triggered();

    void on_process_started();

    void on_process_stdout_ready();

    void on_process_stderr_ready();

    void on_process_finished(int exitCode, QProcess::ExitStatus exitStatus);

    void on_actionAbort_triggered();

private:
    Ui::MainWindow *ui;
    QProcess *process_;
    int term_req_cnt_;
};

#endif // MAINWINDOW_H
