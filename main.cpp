/*
    yaffmpeg - yet another ffmpeg front-end

    Copyright (C) 2018  Martin Preinfalk

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "mainwindow.h"
#include <QApplication>
#include <QTranslator>
#include <QLibraryInfo>
#include <QLocale>
#include <QDebug>
#include <QMessageBox>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QCoreApplication::setOrganizationName("martin.preinfalk");
    QCoreApplication::setApplicationName("yaffmpeg");

    QTranslator qtTranslator;
    if (qtTranslator.load("qt_" + QLocale::system().name(),
                          QLibraryInfo::location(QLibraryInfo::TranslationsPath))) {
        a.installTranslator(&qtTranslator);
        qDebug() << "Qt Translations loaded and translator installed!";
    } else {
        qDebug() << "Qt Translations where not found";
    }

    QTranslator translator;
    if (translator.load(QLocale(), "yaffmpeg", "_", ":translations")) {
        a.installTranslator(&translator);
        qDebug() << "Translations loaded and translator installed!";
    } else {
        qDebug() << "Translations where not found";
    }
    MainWindow w(QCoreApplication::arguments().length() > 1 ? QCoreApplication::arguments().at(1) : "");
    w.show();
    return a.exec();
}
