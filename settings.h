/*
    yaffmpeg - yet another ffmpeg front-end

    Copyright (C) 2018  Martin Preinfalk

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDialog>

#define DEFAULT_CMD "ffmpeg"
#define DEFAULT_ARGS "-i INPUT -itsoffset 0.75 -i INPUT -map 1:v -map 0:a -vcodec mpeg4 -qscale 3 -y OUTPUT"
#define DEFAULT_POSTFIX "_transcoded"

namespace Ui {
class Settings;
}

class Settings : public QDialog
{
    Q_OBJECT

public:
    explicit Settings(QWidget *parent = 0);
    ~Settings();

    QString get_cmd();
    QString get_args();
    QString get_postfix();

    const Ui::Settings* get_ui() const;

private slots:
    void on_pushButton_clicked();

private:
    Ui::Settings *ui;
};

#endif // SETTINGS_H
